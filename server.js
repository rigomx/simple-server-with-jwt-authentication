import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import jwt from "jsonwebtoken";
import { v4 as uuidv4 } from "uuid";
import dotenv from "dotenv";

dotenv.config();
const app = express();

const whitelist = [process.env.CLIENT_URL];

const corsOptions = {
  origin: function (origin, callback) {
    console.log("origin", origin);
    if (whitelist.includes(origin)) {
      callback(null, true);
    } else {
      callback(new Error("Error de Cors"));
    }
  },
};
app.use(cors(corsOptions));

app.use(
  cors({
    methods: ["GET", "POST", "DELETE", "UPDATE", "PUT", "PATCH", "OPTIONS"],
  })
);
// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Encanta Música Client Portal API." });
});

app.post("/api/auth/login", (req, res) => {
  const { email, password } = req.body;

  if (email === process.env.EMAIL_DEFAULT && password === process.env.PASSWORD_DEFAULT) {
    // create a token
    const accessToken = jwt.sign({ userId: uuidv4() }, process.env.JWT_SECRET, {
      expiresIn: "1d",
    });

    // send the response
    res.status(200).json({
      accessToken,
      user: {
        id: "8864c717-587d-472a-929a-8e5f298024da-0",
        displayName: "Jaydon Frankie",
        email: process.env.EMAIL_DEFAULT,
        password: process.env.PASSWORD_DEFAULT,
        photoURL:
          "https://api-dev-minimal-v510.vercel.app/assets/images/avatar/avatar_25.jpg",
        phoneNumber: "+40 777666555",
        country: "United States",
        address: "90210 Broadway Blvd",
        state: "California",
        city: "San Francisco",
        zipCode: "94116",
        about:
          "Praesent turpis. Phasellus viverra nulla ut metus varius laoreet. Phasellus tempus.",
        role: "admin",
        isPublic: true,
      },
    });
  } else {
    res.status(401).json({ error: "Invalid login credentials" });
  }
});

app.get("/", (req, res) => {
  res.send("Simple server with JWT Authentication");
});

app.get("/api", (req, res) => {
  res.send("call to api/auth/login");
});

// start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`Server running on port ${PORT}!`));

