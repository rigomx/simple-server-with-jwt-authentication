# Simple server with JWT Authentication

## Description
This is a simple server with JWT authentication. 
I made it in order to be able to login without the original server in the minimals ReactJS MUI theme.

### Project setup
Install the dependencies
```
npm install
```

### Set up the environment variables
Create a .env file in the root directory and add the following variables
```
JWT_SECRET=your_secret_key
CLIENT_URL=http://localhost:8080
SERVER_PORT=5000
EMAIL_DEFAULT=email@hosting.com
PASSWORD_DEFAULT=demo1234
```

### Run the server
```
node server.js
```

### Test the login endpoint using postman
Use the following endpoint to test the login endpoint
```
http://localhost:5000/api/auth/login
```